import random
hints = dict(
    python = "THIS programming language.",
    jumble = "To shake things up.",
    easy = "The opposite of difficult.",
    difficult = "The opposit of easy",
    answer = "Every question has an...",
    Uplift = "The Tech hub.",
    victor = "The python programmer.",
    Maidugui = "Place of PPA.",
    )
WORDS = list(hints)    

word = random.choice(WORDS)

score = 0

L = list(word)   
random.shuffle(L)  
jumble = ''.join(L)
#Start game
print("""
      Welcome to Word Jumble!
      

      Unscramble the letters to make a word.
      (Press the enter key at the prompt to quit.)
      """)
print("The jumble is:",jumble)
print("To get a hint, type 'hint'")
guess = input("Your guess:")

if guess == "hint":
    score += 1
    print(hints[word])
    guess = input("Your guess:")

while guess and (guess != word) and (guess != "hint"):
    print("Sorry, that's not it.")
    guess = input("Your guess:")

if guess == word and (guess != word) and (guess != "hint"):
    if score:
        print("That's it! You got it!")
        print("Thanks for playing, but there's no reward because you needed help lol.")
    else:
        print("That's it! You guessed it")
        print("AND you needed NO help, Go get your coffee!")
input("Press the enter key to exit.")
